package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public LoginServlet() {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter printWriter = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		printWriter.println("<html>");

		if(emailId.equalsIgnoreCase("STUDENT") && password.equals("STUDENT")){
			printWriter.println("<body bgcolor ='lightblue' text = 'green'> ");
			printWriter.println("<center><h1>Welcome to StudentPage</h1>");
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentHomePage");
			requestDispatcher.forward(request, response);
		}
		else{
			printWriter.println("<body bgcolor ='lightorange' text='green'>");
			printWriter.println("<center><h1>InValid Credentials </h1>");
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
			requestDispatcher.include(request, response);
		}
		printWriter.println("</center>");
		printWriter.println("</body>");
		printWriter.println("</html>");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}